class ItemsController < ApplicationController
  before_action :set_note

  def index
  end

  def new
    @item = @note.items.new
  end

  def create
    @item = @note.items.new(item_params)
    if @item.save
      flash[:success] = "Added note item."
      redirect_to note_items_path
    else
      flash[:error] = "adding item failed."
      render action: :new
    end
  end

  def edit
    @item = @note.items.find(params[:id])
  end

  def update
    @item = @note.items.find(params[:id])
    if @item.update_attributes(item_params)
      flash[:success] = "item has been updated."
      redirect_to note_items_path
    else
      flash[:error] = "update item failed."
      render action: :edit
    end
  end

  def destroy
    @item = @note.items.find(params[:id])
    @item.destroy
    flash[:notice] = "Item has been deleted."
    redirect_to note_items_path
  end

  def completed
    @item = @note.items.find(params[:id])
    @item.update_attributes(completed_at: Time.now)
    redirect_to note_items_path, notice: "Item has been marked as completed"
  end

  private

    def set_note
      @note = Note.find(params[:note_id])
    end

    def item_params
      params.require(:item).permit(:content)
    end
end
