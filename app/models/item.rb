class Item
  include Mongoid::Document
  field :content, type: String
  field :completed_at, type: DateTime

  validates_presence_of :content
  validates_length_of :content, minimum: 3
  embedded_in :note

  scope :complete, -> { where(:completed_at.exists => true ) }
  #scope :incomplete, -> { where(completed_at: nil) }
  scope :incomplete, -> { where(:completed_at.exists => false ) }
  
  def completed?
    !completed_at.blank?
  end
end
