class Note
  include Mongoid::Document
  field :title, type: String
  field :content, type: String

  #validate
  validates_presence_of :title
  validates_length_of :title, minimum: 3
  validates_presence_of :content

  embeds_many :items

  def has_completed_items?
    items.complete.size > 0
  end

  def has_incomplete_items?
    items.incomplete.size > 0
  end
end
