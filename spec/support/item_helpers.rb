module ItemHelpers
  def visit_items(note)
    visit "/"
    within "#note_#{note.id}" do
      click_link "Items"
    end
  end
end