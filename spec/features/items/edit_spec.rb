require "rails_helper"

describe "Edit item" do
  let!(:note) { Note.create(title: "Read Later", content: "Some content for read later!") }
  let!(:item) { note.items.create(content: "Rails") }

  def visit_items(note)
    visit "/"
    within "#note_#{note.id}" do
      click_link "Items"
    end
  end

  it "update successful with valid content" do
    visit_items(note)
    within "#note_item_#{item.id}" do
      click_link "Edit"
    end
    fill_in "Content", with: "Ruby on Rails"
    click_button "Update"
    expect(page).to have_content("item has been updated.")
    item.reload
    expect(item.content).to eql("Ruby on Rails")
  end

  it "update unsuccessful with invalid content" do
    visit_items(note)
    within "#note_item_#{item.id}" do
      click_link "Edit"
    end
    fill_in "Content", with: "R"
    click_button "Update"
    within "div.flash" do
      expect(page).to have_content("update item failed.")
    end
    expect(page).to have_content("Content is too short (minimum is 3 characters)")
  end
end