require "rails_helper"

describe "Completeing item" do
  let!(:note) { Note.create(title: "title", content: "content") }
  let!(:item) { note.items.create(content: "item content.") }

  it "has been completed" do
    visit_items(note)
    expect(item.completed_at).to be_nil
    within "#note_item_#{item.id}" do
      click_link "Complete"
    end
    item.reload
    expect(item.completed_at).not_to be_nil
  end

  context "Item completed" do
    let!(:completed_item) { note.items.create(content: "rails ruby", completed_at: Time.now) }

    it "should show completed time" do
      visit_items(note)
      within "#note_item_#{completed_item.id}" do
        expect(page).to have_content(completed_item.completed_at)
      end
    end

    it "should not display complete link" do
      visit_items(note)
      within "#note_item_#{completed_item.id}" do
        expect(page).not_to have_content("Complete")
      end
    end
  end
end