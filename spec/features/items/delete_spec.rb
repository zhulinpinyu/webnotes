require "rails_helper"

describe "Deleteing item" do
  let!(:note) { Note.create(title: "homework", content: "list homework") }
  let!(:item) { note.items.create(content: "learn To-do list") }

  it "is successful" do
    visit_items(note)
    within "#note_item_#{item.id}" do
      click_link "Delete"
    end
    expect(page).to have_content("Item has been deleted.")
    expect(Item.count).to eql(0)
  end
end