require 'rails_helper'

describe "Adding note's item" do
  let!(:note) { Note.create(title: "Read Later", content: "Some content for read later!") }

  def visit_items(note)
    visit "/"
    within "#note_#{note.id}" do
      click_link "Items"
    end
  end

  it "create success with valid content" do
    visit_items(note)
    click_link "New item"
    fill_in "Content", with: "Ruby"
    click_button "Save"
    expect(page).to have_content("Added note item.")
  end

  it "Display an error with empty content" do
    visit_items(note)
    click_link "New item"
    fill_in "Content", with: ""
    click_button "Save"
    within "div.flash" do
      expect(page).to have_content("adding item failed.")
    end
    expect(page).to have_content("Content can't be blank")
  end

  it "Display an error when content less than 3 characters" do
    visit_items(note)
    click_link "New item"
    fill_in "Content", with: "ml"
    click_button "Save"
    within "div.flash" do
      expect(page).to have_content("adding item failed.")
    end
    expect(page).to have_content("Content is too short (minimum is 3 characters)")
  end

end