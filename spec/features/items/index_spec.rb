require "rails_helper"

describe "Viewing items" do
  let!(:note) { Note.create(title: "my title", content: "my content") }

  def visit_items(note)
    visit "/"
    within "#note_#{note.id}"do
      click_link "Items"
    end
  end

  it "Display note's title" do
    visit_items(note)
    within "h2"do
      expect(page).to have_content(note.title)
    end
  end

  it "Display note's items" do
    item1 = note.items.create(content: "read")
    item2 = note.items.create(content: "film")
    visit_items(note)
    expect(page.all("tbody.note_items tr").size).to eql(2)
    within "tbody.note_items" do
      expect(page).to have_content(item1.content)
      expect(page).to have_content(item2.content)
    end
  end

  it "Display no items when note is empty" do
    visit_items(note)
    expect(page.all("tbody.note_items tr").size).to eql(0)
  end
end