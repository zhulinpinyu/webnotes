require 'rails_helper'

describe "Creating notes" do
  def create_note(option={})
    option[:title] ||= "my first note"
    option[:content] ||= "my content"
    visit "/"
    click_link "New Note"
    expect(page).to have_content("New note")

    fill_in "Title", with: option[:title]
    fill_in "Content", with: option[:content]
    click_button "Create Note"
  end

  it "redirect to show page when create success" do
    create_note
    expect(page).to have_content("my first note")
  end

  it "Display an error when note's title is empty" do
    create_note title: ""
    expect(page).to have_content("Title can't be blank")
    expect(Note.count).to eql(0)
  end

  it "Display an error when title's length less 3 characters" do
    create_note title: "we"
    expect(page).to have_content("Title is too short (minimum is 3 characters)")
    expect(Note.count).to eql(0)
  end

  it "Display an error when content is empty" do
    create_note content: ""
    expect(page).to have_content("Content can't be blank")
    expect(Note.count).to eql(0)
  end
end