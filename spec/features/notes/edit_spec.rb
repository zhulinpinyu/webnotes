require "rails_helper"

describe "Editing notes" do
  let!(:note) { Note.create(title: "today", content: "today is mi huhu.") }

  def update_note(option={})
    option[:title] ||= "my eye"
    option[:content] ||= "today eye is very bad."
    note = option[:note]
    visit "/"
    within "#note_#{note.id}" do
      click_link "Edit"
    end
    fill_in "Title", with: option[:title]
    fill_in "Content", with: option[:content]
    click_button "Update Note"
    note.reload
  end

  it "update successful with correct infomation" do
    update_note note: note
    expect(page).to have_content("Note was successfully updated.")
    expect(note.title).to eql("my eye")
    expect(note.content).to eql("today eye is very bad.")
  end

  it "Display Error with empty title" do
    update_note title: "", note: note
    expect(page).to have_content("Title can't be blank")
  end

  it "Display Error when title less 3 characters" do
    update_note title: "Hi", note: note
    expect(page).to have_content("Title is too short (minimum is 3 characters)")
  end

  it "Display Error when content is empty" do
    update_note content: "", note: note
    expect(page).to have_content("Content can't be blank")
  end
end