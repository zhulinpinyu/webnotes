require "rails_helper"

describe "Destroy note" do
  let!(:note) { Note.create(title: "zlpy title", content: "zlpy") }

  it "is successful when click destroy link" do
    visit "/"
    within "#note_#{note.id}" do
      click_link "Destroy"
    end

    expect(page).to_not have_content(note.title)
    expect(page).to_not have_content(note.content)
    expect(Note.count).to eql(0)
  end
end