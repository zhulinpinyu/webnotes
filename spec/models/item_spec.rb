require 'rails_helper'

describe Item do
  it { should be_embedded_in(:note) }

  describe "#completed?" do
    let(:note) { Note.create(title: "title", content: "content") }
    let(:item) { note.items.create(content: "content") }

    it "is false when item unmark completed" do
      item.completed_at = nil
      #expect(item.completed?).to be_falsey
      expect(item.completed?).to be false
    end

    it "return true when item is marked completed_at" do
      item.completed_at = Time.now
      #expect(item.completed?).to be_truthy
      expect(item.completed?).to be true
    end
  end
end
