require 'rails_helper'

describe Note do
  it { should embed_many(:items) }

  describe "#has_completed_items?" do
    let(:note) { Note.create(title: "title", content: "content") }

    it "return false when complete item is empty" do
      expect(note.has_completed_items?).to be false
    end

    it "return true when note has completed item" do
      note.items.create(content: "content", completed_at: Time.now)
      expect(note.has_completed_items?).to be true
    end

    it "return true when note has incomplete item" do
      note.items.create(content: "content")
      expect(note.has_incomplete_items?).to be true
    end
  end
end
